import json, requests
import subprocess

fetch_files_url = 'http://127.0.0.1:8888/getoldfilenames'

resp = requests.post(url=fetch_files_url)
data = json.loads(resp.text)
for file in data:
    subprocess.call(['avconv', '-i', file['file_name'], '-c:v', 'libx264', '-s', '320x240', '-b:v', '1M', '-c:a', 'aac',
                    file['file_name'] ])